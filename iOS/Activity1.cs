﻿using System;

namespace DropCoordinates.iOS
{
	public class Activity1 : CMMotionManager
	{
		public Activity1()
		{
		}
		int count = 1;
		private Android.Hardware.SensorManager sm;
		private bool sersorrunning;
		private CompassView compView;
		private Sensor s;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.Main);
			var layout = FindViewById<LinearLayout>(Resource.Id.ll);
			var Params = new Android.Widget.LinearLayout.LayoutParams(ViewGroup.LayoutParams.FillParent, ViewGroup.LayoutParams.FillParent);

			compView = new CompassView(this, null);
			layout.AddView(compView, Params);

			sm = (Android.Hardware.SensorManager)GetSystemService(Context.SensorService);

			IList<Sensor> mySensors = sm.GetSensorList(SensorType.Orientation);

			if (mySensors.Count > 0)
			{
				s = mySensors[0];
				sm.RegisterListener(this, mySensors[0], SensorDelay.Normal);
				sersorrunning = true;
				Toast.MakeText(this, "Start ORIENTATION Sensor", ToastLength.Long).Show();

			}
			else
			{
				Toast.MakeText(this, "No ORIENTATION Sensor", ToastLength.Long).Show();
				sersorrunning = false;
				Finish();
			}
		}

		public void OnAccuracyChanged(Sensor sensor, Android.Hardware.SensorStatus accuracy)
		{
		}

		public void OnSensorChanged(SensorEvent e)
		{
			if (e.Sensor.Type == SensorType.Orientation)
			{
				float dir = e.Values[0];
				compView.updateDirection(dir);
			}
		}

		protected override void OnPause()
		{
			base.OnPause();
			sm.UnregisterListener(this);
		}
		protected override void OnResume()
		{
			base.OnResume();
			sm.RegisterListener(this, s, SensorDelay.Normal);
		}
		protected override void OnDestroy()
		{
			Base.OnDestroy();
			sm.UnregisterListener(this);
		}
	}
}
