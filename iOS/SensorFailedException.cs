﻿using System;
namespace DropCoordinates.iOS
{
	public class SensorFailedException : Exception
	{
		public int ErrorId { get; protected set; }

		internal SensorFailedException(string message)
			: base(message)
		{
		}
	}
}

