﻿using System;
using System.Json;
using CoreLocation;
using CoreMotion;
using Foundation;
using Foundation;
using UIKit;
using Wikitude.Architect;
using OpenTK;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace DropCoordinates.iOS
{
	public class ApplicationModelARViewController : UIViewController ,IDeviceMotion, IDisposable
	{  private double ms = 1000.0;
		protected WTArchitectView arView;
		public delegate void Action<WTStartupConfiguration>(WTStartupConfiguration startUpConfig);
		public string WorldOrUrl { get; private set; }
		protected IDictionary<MotionSensorType, bool> sensorStatus;
		public CLLocationManager locationManager;
		public CLLocationManager locationM;
		protected CLLocation location;
		public CMMotionManager motionManager;
		protected CMMotionManager mManager;
		protected double Azimuth;
		protected double Inclination;
		protected JsonArray poiData;
		protected NSError error;
		private List<float[]> mRotHist ;
		private int mRotHistIndex;
		private int mHistoryMaxLength = 40;
		private float[] rMat;
		public bool IsDataValid { get; protected set; }
		double d;

		public ApplicationModelARViewController(string worldOrUrl) : base()
		{
			WorldOrUrl = worldOrUrl;

			this.location = new CLLocation();
			motionManager = new CMMotionManager();
			locationManager = new CLLocationManager();
			mManager= new CMMotionManager();
			locationM = new CLLocationManager();
			this.poiData = new JsonArray();
			this.Azimuth = new double();
			this.Inclination = new double();
			mRotHist = new List<float[]>();
			locationManager.DesiredAccuracy = CLLocation.AccuracyBest;
			locationManager.HeadingFilter = 1;
			sensorStatus = new Dictionary<MotionSensorType, bool>(){
				{ MotionSensorType.Accelerometer, false},
				{ MotionSensorType.Gyroscope, false},
				{ MotionSensorType.Magnetometer, false},
				{ MotionSensorType.Compass, false}
			};
			this.d = new double();
		}

	 async public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			if (UIDevice.CurrentDevice.CheckSystemVersion(7, 0))
			{
				EdgesForExtendedLayout = UIRectEdge.None;
			}

			var requiredFeatures = WTFeatures.Geo;

			if (WTArchitectView.IsDeviceSupportedForRequiredFeatures(requiredFeatures, out error))
			{
				arView = new WTArchitectView(UIScreen.MainScreen.Bounds, mManager);
				arView.SetLicenseKey("CZq0yviTba5JCP8AIGtw8i3lCUHyQDESdSwha178QAZOd3/AlEJ0DuvXBy38L+pQ8wfKBfxH/rMuqu4UHeIVuc/Oa2sC0YBPq1VtusoX9uRhigWR8m96a2y9pLyu/PLzJoyuXWZzeMv/Gsg6yei0/6xURHWO9tW3jTdYhPZpw7NTYWx0ZWRfX+1J7PqNQ1sNzC+gnJrkA4MU6LPZaUFpSjdikdZeuIORcpzTY7JL/fOEvwU3fT+UGLh4pX8TgeOh8o2qi98lS6U2mG3ApIjVRVSBSMMhMVcBB9spXtt07eB7vJhg5mr2hWHnGaP+jge9kBj/D781W5CdjonJ8SrTmS3+tkOBB2YZNbDfdYyAAjXNOs5WMGC3r3hVAkNt+HXZvFe/00czUCun+VN+EeLg/dgLuPQ1U5DwIPa+ZQaNq7RNGUQCy4egbaEGawVPzZlDJbUo9k3riitBz5m10HzfyJZ60jKOR69gzo4ZAPR9MaNEN7SFblrS7dZdgNN3fDh/we9H2NoBXEybyvo4yjKSkDKayCvLRIQZJzb91T4nEUBC3qG9Y7UHNpLpiVq7qkuytc5PIYVSkkSykJMPX0Z7bzetCL1fJ7EJR2FHY6byg8CbiDua4zD6oWbRjrADHllwkYjaxxskLo+CuKtCne5lGQ2uxmL2h53DO0d2aIwKyOs=");


				WorldOrUrl = NSBundle.MainBundle.BundleUrl.AbsoluteString + "ArchitectExamples/" + WorldOrUrl + "/index.html";

				var u = new NSUrl(WorldOrUrl);
				arView.LoadArchitectWorldFromURL(u, requiredFeatures);
				View.AddSubview(arView);

				//------------------------------


				this.locationManager.StartUpdatingLocation();
				location = locationManager.Location;
				this.locationManager.StopUpdatingLocation();

				Console.WriteLine(CLLocationManager.Status);

				await Task.Delay(1000);
				Start(MotionSensorType.Compass, MotionSensorDelay.Default);
				await Task.Delay(1000);
				await Task.Delay(1000);
				Console.WriteLine(d + "hhhhhhhhh");

			
					motionManager.StartDeviceMotionUpdates(CMAttitudeReferenceFrame.XTrueNorthZVertical, NSOperationQueue.CurrentQueue, (motion, error) =>
					{  
						mManager = motionManager;
						Console.WriteLine(d + "kkkkkkkkkkkhhhhhh");


						CMAcceleration gravity = mManager.DeviceMotion.Gravity;
						Vector2 vect2 = new Vector2((float)-gravity.X, (float)-gravity.Y);


						Console.WriteLine(d + "lllllllll");
						double tiltAngle = angleYAxisToVector(vect2);
						double newHeading = (d + tiltAngle) % 360;
						Console.WriteLine(newHeading + "ooooooool");
						this.Inclination = 0;
						this.poiData = GeoUtils.GetPoiInformation(this.location, 1, newHeading, this.Inclination);
						Console.WriteLine(Azimuth);
						var js = "World.loadPoisFromJsonData(" + this.poiData.ToString() + ")";
						this.arView.CallJavaScript(js);

						motionManager.StopDeviceMotionUpdates();


					});

					

				if (arView != null)
				{
					arView.Start((startupConfiguration) =>
				   {
					   startupConfiguration.CaptureDevicePosition = AVFoundation.AVCaptureDevicePosition.Back;
				   }, (isRunning, error) =>
				   {
					   if (isRunning)
					   {
						   Console.WriteLine("Wikitude SDK version " + WTArchitectView.SDKVersion + " is running.");
					   }
					   else
					   {
						   Console.WriteLine("Unable to start Wikitude SDK. Error: " + error.LocalizedDescription);
					   }
				   });
				}
			}
			else
			{
				var adErr = new UIAlertView("Unsupported Device", "This device is not capable of running ARchitect Worlds. Requirements are: iOS 5 or higher, iPhone 3GS or higher, iPad 2 or higher. Note: iPod Touch 4th and 5th generation are only supported in WTARMode_IR.", null, "OK", null);
				adErr.Show();
			}

		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
		}

		override public void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

		}
		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			if (arView != null)
				arView.Stop();
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);
			StopUpdatesWithMotionType();
		}

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

	//public void StartUpdatesWithMotionDataType()
	//	{
	//		CMMotionManager cmmotionManager = new CMMotionManager();
	//		cmmotionManager.Init();
	//		CLLocationManager loc = new CLLocationManager();
	//		loc.Init();
		
	//		try
	//		{
				
	//			cmmotionManager.ShowsDeviceMovementDisplay = true;
	//			cmmotionManager.DeviceMotionUpdateInterval = 1.0;

	//			cmmotionManager.StartDeviceMotionUpdates(CMAttitudeReferenceFrame.XTrueNorthZVertical, NSOperationQueue.CurrentQueue, (motion, error) =>
	//			{ this.IsDataValid = error == null;
	//			if (this.IsDataValid)
	//			{
						

	//				this.mManager = cmmotionManager;
	//				 loc = locationManager;
	//				loc.StartUpdatingHeading();


	//				CMAcceleration gravity = mManager.DeviceMotion.Gravity;
	//				Vector2 vect2 = new Vector2((float)-gravity.X,(float) -gravity.Y);

	//				Task.Delay(800);
	//			 CLHeading r = loc.Heading;

					//double a =r.MagneticHeading;
					//Console.WriteLine(a + "lllllllll");
					//double tiltAngle = angleYAxisToVector(vect2);
					//double newHeading = (a + tiltAngle)%360;
					//Console.WriteLine( newHeading+ "ooooooool");

					//var r = this.mManager.DeviceMotion.Attitude.RotationMatrix;

					//	rMat = new float[9];
					//	rMat[0] = (float)r.m11;
					//	rMat[1] = (float)r.m12;
					//	rMat[2] = (float)r.m13;
					//	rMat[3] = (float)r.m21;
					//	rMat[4] = (float)r.m22;
					//	rMat[5] = (float)r.m23;
					//	rMat[6] = (float)r.m31;
					//	rMat[7] = (float)r.m32;
					//	rMat[8] = (float)r.m33;

					//	setRotHist();

					//	Azimuth = (int)(findFacing() * 180 / System.Math.PI + 360) % 360;



							//this.Inclination = 0;
							//this.poiData = GeoUtils.GetPoiInformation(this.location,1,newHeading, this.Inclination);
							//Console.WriteLine(Azimuth);
							//var js = "World.loadPoisFromJsonData(" + this.poiData.ToString() + ")";
							//this.arView.CallJavaScript(js);
					//StopUpdatesWithMotionType();
		//			}

		//		});
		//	}
		//	catch (Exception e)
		//	{
		//		Console.WriteLine(e.ToString());
		//	}
		//}

		public void StopUpdatesWithMotionType()
		{
			this.mManager.StopDeviceMotionUpdates();
		}
		private void clearRotHist()
		{
			mRotHist.Clear();
			mRotHistIndex = 0;
		}

		private void setRotHist()
		{
			float[] hist = rMat;
			int t = mRotHist.Count;
			if (t == mHistoryMaxLength)
			{
				mRotHist.RemoveAt(mRotHistIndex);
			}
			mRotHist.Insert(mRotHistIndex++, hist);
			mRotHistIndex %= mHistoryMaxLength;
		}

		private float findFacing()
		{
			float[] averageRotHist = average(mRotHist);
			return (float)System.Math.Atan2(averageRotHist[1], averageRotHist[4]);
		}

		public float[] average(List<float[]> values)
		{
			float[] result = new float[9];
			foreach (float[] value in values)
			{
				for (int i = 0; i < 9; i++)
				{
					result[i] += value[i];
				}
			}

			for (int i = 0; i < 9; i++)
			{
				int p = values.Count;
				result[i] = result[i] / p;
			}

			return result;
		}

		public static double angleYAxisToVector(Vector2 v)
		{double dX = v.X;
			double dY = v.Y;

			if (dY == 0)
			{
				if (dX > 0)
				{
					return 0.0;
				}
				else {
					if (dX < 0)
					{
						return 180.0;
					}
					else {
						return -1;
					}
				}
			}

			double beta = Math.Atan(dX / dY)* 180.0 / Math.PI;
			double angle;
			if (dX > 0)
			{
				if (dY < 0)
				{
					angle = 180 + beta;
				}
				else {
					angle = beta;
				}
			}
			else {
				if (dY < 0)
				{
					angle = 180 + beta;
				}
				else {
					angle = 360 + beta;
				}
			}
			//    NSLog(@"angle = %f, normalized = %f",beta,angle);
			return angle;
		}
		#region IDeviceMotion implementation
		/// <summary>
		/// Occurs when sensor value changed.
		/// </summary>
		public event SensorValueChangedEventHandler SensorValueChanged;

		/// <summary>
		/// Start the specified sensorType and interval.
		/// </summary>
		/// <param name="sensorType">Sensor type.</param>
		/// <param name="interval">Interval.</param>
		public void Start(MotionSensorType sensorType, MotionSensorDelay interval)
		{

			switch (sensorType)
			{
				case MotionSensorType.Accelerometer:
					if (motionManager.AccelerometerAvailable)
					{
						motionManager.AccelerometerUpdateInterval = (double)interval / ms;
						motionManager.StartAccelerometerUpdates(NSOperationQueue.CurrentQueue, OnAccelerometerChanged);
					}
					else
					{
						Debug.WriteLine("Accelerometer not available");
					}
					break;
				case MotionSensorType.Gyroscope:
					if (motionManager.GyroAvailable)
					{
						motionManager.GyroUpdateInterval = (double)interval / ms;
						motionManager.StartGyroUpdates(NSOperationQueue.CurrentQueue, OnGyroscopeChanged);
					}
					else
					{
						Debug.WriteLine("Gyroscope not available");
					}
					break;
				case MotionSensorType.Magnetometer:
					if (motionManager.MagnetometerAvailable)
					{
						motionManager.MagnetometerUpdateInterval = (double)interval / ms;
						motionManager.StartMagnetometerUpdates(NSOperationQueue.CurrentQueue, OnMagnometerChanged);
					}
					else
					{
						Debug.WriteLine("Magnetometer not available");
					}
					break;
				case MotionSensorType.Compass:
					if (CLLocationManager.HeadingAvailable)
					{
						locationManager.StartUpdatingHeading();
						locationManager.UpdatedHeading += OnHeadingChanged;
					}
					else
					{
						Debug.WriteLine("Compass not available");
					}
					break;
			}
			sensorStatus[sensorType] = true;
		}

		private void OnHeadingChanged(object sender, CLHeadingUpdatedEventArgs e)
		{
			if (SensorValueChanged == null)
			{
			 this.d = e.NewHeading.TrueHeading;
				return;
			}
			SensorValueChanged(this, new SensorValueChangedEventArgs { ValueType = MotionSensorValueType.Single, SensorType = MotionSensorType.Compass, Value = new MotionValue { Value = e.NewHeading.TrueHeading } });
			this.d = e.NewHeading.TrueHeading;
		}

		/// <summary>
		/// Raises the magnometer changed event.
		/// </summary>
		/// <param name="data">Data.</param>
		/// <param name="error">Error.</param>
		private void OnMagnometerChanged(CMMagnetometerData data, NSError error)
		{
			if (SensorValueChanged == null)
				return;

			SensorValueChanged(this, new SensorValueChangedEventArgs { ValueType = MotionSensorValueType.Vector, SensorType = MotionSensorType.Magnetometer, Value = new MotionVector() { X = data.MagneticField.X, Y = data.MagneticField.Y, Z = data.MagneticField.Z } });

		}

		/// <summary>
		/// Raises the accelerometer changed event.
		/// </summary>
		/// <param name="data">Data.</param>
		/// <param name="error">Error.</param>
		private void OnAccelerometerChanged(CMAccelerometerData data, NSError error)
		{
			if (SensorValueChanged == null)
				return;

			SensorValueChanged(this, new SensorValueChangedEventArgs { ValueType = MotionSensorValueType.Vector, SensorType = MotionSensorType.Accelerometer, Value = new MotionVector() { X = data.Acceleration.X, Y = data.Acceleration.Y, Z = data.Acceleration.Z } });

		}

		/// <summary>
		/// Raises the gyroscope changed event.
		/// </summary>
		/// <param name="data">Data.</param>
		/// <param name="error">Error.</param>
		private void OnGyroscopeChanged(CMGyroData data, NSError error)
		{
			if (SensorValueChanged == null)
				return;

			SensorValueChanged(this, new SensorValueChangedEventArgs { ValueType = MotionSensorValueType.Vector, SensorType = MotionSensorType.Gyroscope, Value = new MotionVector() { X = data.RotationRate.x, Y = data.RotationRate.y, Z = data.RotationRate.z } });

		}

		public void Stop(MotionSensorType sensorType)
		{
			switch (sensorType)
			{
				case MotionSensorType.Accelerometer:
					if (motionManager.AccelerometerActive)
						motionManager.StopAccelerometerUpdates();
					else
						Debug.WriteLine("Accelerometer not available");
					break;
				case MotionSensorType.Gyroscope:
					if (motionManager.GyroActive)
						motionManager.StopGyroUpdates();
					else
						Debug.WriteLine("Gyroscope not available");
					break;
				case MotionSensorType.Magnetometer:
					if (motionManager.MagnetometerActive)
						motionManager.StopMagnetometerUpdates();
					else
						Debug.WriteLine("Magnetometer not available");
					break;
				case MotionSensorType.Compass:
					if (CLLocationManager.HeadingAvailable)
					{
						locationManager.StopUpdatingHeading();
						locationManager.UpdatedHeading -= OnHeadingChanged;
					}
					else
					{
						Debug.WriteLine("Compass not available");
					}
					break;
			}
			sensorStatus[sensorType] = false;
		}

		/// <summary>
		/// Determines whether this instance is active the specified sensorType.
		/// </summary>
		/// <returns><c>true</c> if this instance is active the specified sensorType; otherwise, <c>false</c>.</returns>
		/// <param name="sensorType">Sensor type.</param>
		public bool IsActive(MotionSensorType sensorType)
		{
			return sensorStatus[sensorType];
		}

		#endregion





	
			
	

	}
}
