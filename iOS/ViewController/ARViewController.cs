﻿using System;
using UIKit;
using Foundation;
using MonoTouch.Dialog;
using Wikitude.Architect;
using CoreMotion;

namespace DropCoordinates.iOS
{
	public class ARViewController : UIViewController
	{
		protected WTArchitectView arView;

		public delegate void Action<WTStartupConfiguration>(WTStartupConfiguration startUpConfig);

		public string WorldOrUrl { get; private set; }
		public bool IsUrl { get; private set; }

		public ARViewController(string worldOrUrl, bool isUrl) : base()
		{
			WorldOrUrl = worldOrUrl;
			IsUrl = isUrl;
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			if (UIDevice.CurrentDevice.CheckSystemVersion(7, 0))
			{
				EdgesForExtendedLayout = UIRectEdge.None;
			}

			var requiredFeatures = WTFeatures.Geo;
			var error = new NSError();

			if (WTArchitectView.IsDeviceSupportedForRequiredFeatures(requiredFeatures, out error))
			{
				var motion = new CMMotionManager();
				arView = new WTArchitectView(UIScreen.MainScreen.Bounds, motion);
				arView.SetLicenseKey("CZq0yviTba5JCP8AIGtw8i3lCUHyQDESdSwha178QAZOd3/AlEJ0DuvXBy38L+pQ8wfKBfxH/rMuqu4UHeIVuc/Oa2sC0YBPq1VtusoX9uRhigWR8m96a2y9pLyu/PLzJoyuXWZzeMv/Gsg6yei0/6xURHWO9tW3jTdYhPZpw7NTYWx0ZWRfX+1J7PqNQ1sNzC+gnJrkA4MU6LPZaUFpSjdikdZeuIORcpzTY7JL/fOEvwU3fT+UGLh4pX8TgeOh8o2qi98lS6U2mG3ApIjVRVSBSMMhMVcBB9spXtt07eB7vJhg5mr2hWHnGaP+jge9kBj/D781W5CdjonJ8SrTmS3+tkOBB2YZNbDfdYyAAjXNOs5WMGC3r3hVAkNt+HXZvFe/00czUCun+VN+EeLg/dgLuPQ1U5DwIPa+ZQaNq7RNGUQCy4egbaEGawVPzZlDJbUo9k3riitBz5m10HzfyJZ60jKOR69gzo4ZAPR9MaNEN7SFblrS7dZdgNN3fDh/we9H2NoBXEybyvo4yjKSkDKayCvLRIQZJzb91T4nEUBC3qG9Y7UHNpLpiVq7qkuytc5PIYVSkkSykJMPX0Z7bzetCL1fJ7EJR2FHY6byg8CbiDua4zD6oWbRjrADHllwkYjaxxskLo+CuKtCne5lGQ2uxmL2h53DO0d2aIwKyOs=");

				var absoluteWorldUrl = WorldOrUrl;
				if (!IsUrl)
					absoluteWorldUrl = NSBundle.MainBundle.BundleUrl.AbsoluteString + "ArchitectExamples/" + WorldOrUrl + "/index.html";
				
				var u = new NSUrl(absoluteWorldUrl);
				arView.LoadArchitectWorldFromURL(u, requiredFeatures);
				View.AddSubview(arView);
			}
			else
			{
				var adErr = new UIAlertView("Unsupported Device", "This device is not capable of running ARchitect Worlds. Requirements are: iOS 5 or higher, iPhone 3GS or higher, iPad 2 or higher. Note: iPod Touch 4th and 5th generation are only supported in WTARMode_IR.", null, "OK", null);
				adErr.Show();
			}
		}

		/*
		 	public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			if (arView != null)
			{
				arView.Start((startupConfiguration) =>
			   {
				   startupConfiguration.CaptureDevicePosition = AVFoundation.AVCaptureDevicePosition.Back;
			   }, (isRunning, error) =>
			   {
				   if (isRunning)
				   {
					   Console.WriteLine("Wikitude SDK version " + WTArchitectView.SDKVersion + " is running.");
				   }
				   else
				   {
					   Console.WriteLine("Unable to start Wikitude SDK. Error: " + error.LocalizedDescription);
				   }
			   });
			}
		}
		*/

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			if (arView != null)
				arView.Stop();
		}

	}
}
