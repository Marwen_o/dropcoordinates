﻿using System;
namespace DropCoordinates.iOS

	{
	/// <summary>
	/// Motion sensor type enum.
	/// </summary>
	public enum MotionSensorType
	{
		/// <summary>
		/// Accelerometer Sensor
		/// </summary>
		Accelerometer,
		/// <summary>
		/// Gyroscope Sensor
		/// </summary>
		Gyroscope,
		/// <summary>
		/// Magnetometer Sensor
		/// </summary>
		Magnetometer,
		/// <summary>
		/// Compass Sensor
		/// </summary>
		Compass
	}
}


