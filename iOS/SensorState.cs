﻿using System;
namespace DropCoordinates.iOS
{
	public enum SensorState
	{
		NotSupported,
		Ready,
		Initializing,
		NoData,
		NoPermissions,
		Disabled
	}
}

