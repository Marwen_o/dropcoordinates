﻿using System;
namespace DropCoordinates.iOS
{
	public interface ISensorReading
	{
		DateTimeOffset Timestamp { get; }
	}
}
