﻿using System;
using System.Collections.Generic;
using Foundation;
using CoreMotion;
using UIKit;

namespace DropCoordinates.iOS
{
	public partial class GraphViewController : UIViewController
	{
		CMMotionManager mManager;
		const double deviceMotionMin = 0.01;

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
			//Delegate has to be called
			AppDelegate appDelegate = (AppDelegate)UIApplication.SharedApplication.Delegate;
			mManager = new CMMotionManager();

		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			StartUpdatesWithMotionDataType();
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);
			StopUpdatesWithMotionType();
		}

		public void StartUpdatesWithMotionDataType()
		{
			try
			{
				mManager.ShowsDeviceMovementDisplay = true;
				mManager.DeviceMotionUpdateInterval = 1.0 / 60.0;
				mManager.StartDeviceMotionUpdates(
					CMAttitudeReferenceFrame.XTrueNorthZVertical,
					NSOperationQueue.CurrentQueue, (motion, error) =>
					{
						var calang = 180 / Math.PI;
						var pitch = motion.Attitude.Pitch * calang;
						var roll = motion.Attitude.Roll * calang;
						var azimuth = motion.Attitude.Yaw * calang;
						
					});
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine("CMDeviceMotion Not Available");
			}
		}

		public void StopUpdatesWithMotionType()
		{
				mManager.StopAccelerometerUpdates();
		}
	}
}
