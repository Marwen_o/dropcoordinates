﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace DropCoordinates.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		public override UIWindow Window { get; set; }
		UINavigationController navController;
		ApplicationModelARViewController viewController;

		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			var jsPath = "4_ObtainPoiData_1_FromApplicationModel";
			Window = new UIWindow(UIScreen.MainScreen.Bounds);
			viewController = new ApplicationModelARViewController(jsPath);
			navController = new UINavigationController(viewController);
			Window.RootViewController = navController;
			Window.MakeKeyAndVisible();

			return true;
		}

	}
}
