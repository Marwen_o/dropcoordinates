﻿using System;
namespace DropCoordinates.iOS
{
	public enum MotionSensorValueType
	{
		/// <summary>
		/// Single value.
		/// </summary>
		Single,
		/// <summary>
		/// Vector value.
		/// </summary>
		Vector
	}
}
