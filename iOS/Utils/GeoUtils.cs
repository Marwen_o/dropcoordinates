﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Json;
using CoreLocation;

namespace DropCoordinates.iOS
{
	
	public class GeoUtils
	{
		const double EarthRadius = 6371000;
		const double distance = 20;
		public static JsonArray GetPoiInformation(CLLocation userLocation, int numberOfPlaces, double Azim, double Inc)
		{
			if (userLocation == null)
				return null;

			numberOfPlaces = 1;
			var alt = userLocation.Altitude;

			var pois = new List<JsonObject>();

			for (int i = 0; i < numberOfPlaces; i++)
			{
				var loc = POICalculation(userLocation.Coordinate.Latitude, userLocation.Coordinate.Longitude, alt, Azim, Inc);

				var p = new Dictionary<string, JsonValue>(){
					{ "id", i.ToString() },
					{ "name", "POI#" + i.ToString() },
					{ "description", "This is the description of POI#" + i.ToString() },
					{ "latitude", loc[0] },
					{ "longitude", loc[1] },
					{ "altitude", loc[2] }
				};

				pois.Add(new JsonObject(p.ToList()));
			}

			var vals = from p in pois select (JsonValue)p;

			return new JsonArray(vals);
		}

		static double[] POICalculation(double lat, double lon, double alti, double az, double incli)
		{
			az = az * Math.PI / 180;
			Console.WriteLine("POI Calculation begins");
			Console.WriteLine("lat = " + lat + ",lng = " + lon + ", azimuth = " + az + ", inclination = " + incli);

			var b = distance / EarthRadius;
			var a = Math.Acos(Math.Cos(b) * Math.Cos((90 - lat) * Math.PI / 180) + Math.Sin((90 - lat) * Math.PI / 180) * Math.Sin(b) * Math.Cos(az));
			var B = Math.Asin(Math.Sin(b) * Math.Sin(az) / Math.Sin(a));

			var newLat = 90 - (a * 180 / Math.PI);
			var newLng = (B * 180 / Math.PI) + lon;
			var newAlt = alti + Math.Tan(incli) * distance;
			Console.WriteLine("NewLat = " + newLat + ", newLng = " + newLng + ", newAlt = " + newAlt);

			return new double[] { newLat, newLng, newAlt };
		}
	}
}
